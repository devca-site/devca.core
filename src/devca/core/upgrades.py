from plone import api
import logging

PROFILE_ID = 'profile-devca.core:default'


def upgrade_types(context, logger=None):
    """Method to upgrade types"""

    if logger is None:
        # Called as upgrade step: define our own logger.
        logger = logging.getLogger('devca.core')

    # Run the catalog.xml step as that may have defined new metadata
    # columns.  We could instead add <depends name="catalog"/> to
    # the registration of our import step in zcml, but doing it in
    # code makes this method usable as upgrade step as well.
    # Remove these lines when you have no catalog.xml file.
    setup = api.portal.get_tool('portal_setup')
    setup.runImportStepFromProfile(PROFILE_ID, 'typeinfo')

    logger.info('updated types')