# -*- coding: utf-8 -*-
from Products.CMFPlone.interfaces import INonInstallable
from Products.CMFPlone.interfaces import constrains
from zope.interface import implementer
from plone.app.textfield.value import RichTextValue
from zope.interface import implementer
from plone import api
from plone.namedfile.file import NamedBlobImage
import os

@implementer(INonInstallable)
class HiddenProfiles(object):

    def getNonInstallableProfiles(self):
        """Hide uninstall profile from site-creation and quickinstaller"""
        return [
            'devca.core:uninstall',
        ]


def post_install(context):
    """Post install script"""
    if context.readDataFile('devcacore_default.txt') is None:
        return
    # Do something during the installation of this package
    portal = api.portal.get()
    if api.content.get('/Members'):
        api.content.delete(api.content.get('/Members'))
    # api.content.delete(api.content.get('/front-page'))
     # disable nonfolderish tabs
    api.portal.set_registry_record('plone.nonfolderish_tabs', False)
    if not api.content.get('/blog'):
        api.content.rename(obj=portal['news'], new_id='blog')
        blog = portal['blog']
        blog.title = 'Blog'
        # api.content.transition(blog, transition='publish')
        behavior = constrains.ISelectableConstrainTypes(blog)
        behavior.setConstrainTypesMode(constrains.ENABLED)
        behavior.setLocallyAllowedTypes(['File',
                                         'Event',
                                         'Folder',
                                         'Image',
                                         'News Item',
                                         'Link',
                                         'Collection'])
        behavior.setImmediatelyAddableTypes(['News Item',
                                         'Image',
                                         'Link',
                                         'Event',
                                         'Folder'])
    create_frontpage(portal)
    create_slider_images(portal)
    create_events(portal)
    create_schedule_page(portal)

def uninstall(context):
    """Uninstall script"""
    if context.readDataFile('devcacore_uninstall.txt') is None:
        return
    # Do something during the uninstallation of this package
    
def create_events(portal):
    event_list = [
        {"title":"Jamaica",
            "description":"Jamaica Event",
            "contact_organization":"MBSS",
            "start":"",
            "end":"",
            "location":"Jamaica",
            "contact_name":"Matthew McNaughton",
            "contact_email":"blah@blah.com",
            "url":"http://blah.com",
            "text":"<strong>this is richtext</strong> right here"
        },
        ]
    events = api.content.get('/events')
    for event_data in event_list:
        event = api.content.create(
                type='Event',
                id=event_data['title'],
                container=events,
                **event_data)
        api.content.transition(event, transition='publish')
def create_slider_images(portal):
    img_path = "browser/static/images"
    
    print "setuphandlers path: ", os.path.dirname(os.path.realpath(__file__))
    title = u"Carousel Images"
    description = u"Images for the DevCa Carousel"
    if api.content.get('/slider-images'):
        return
    slider_images_folder = api.content.create(
            type='Folder',
            id="slider-images",
            title=title,
            description=description,
            container=portal)
    image_srcs = [
            {'image':'carousel_home.jpg',
             'title':'May 5th & 6th',
             'subtitle':'Towards A Caribbean Data Revolution',
             'link':'https://www.eventbrite.com/e/developing-the-caribbean-jamaica-conference-and-codesprint-registration-24791090820'
            },
            {'image':'carousel_home_2.jpg',
             'title':'May 5th & 6th',
             'subtitle':'Towards A Caribbean Data Revolution',
             'link':'https://www.eventbrite.com/e/developing-the-caribbean-jamaica-conference-and-codesprint-registration-24791090820'
            },
            {'image':'carousel_home_3.jpg',
             'title':'May 5th & 6th',
             'subtitle':'Towards A Caribbean Data Revolution',
             'link':'https://www.eventbrite.com/e/developing-the-caribbean-jamaica-conference-and-codesprint-registration-24791090820'
            }
    ]
    dir = os.path.dirname(os.path.realpath(__file__))
    src_folder = os.path.join(dir, 'browser/static/images')
    for item in image_srcs:
        create_image(slider_images_folder, src_folder, item)        
    slider_images_folder.reindexObject()
        
def create_frontpage(portal):
    frontpage_id = 'front-page'
    if frontpage_id not in portal.keys():
        title = u"Welcome to DevCa"
        description = u"Developing the Caribbean"
        frontpage = api.content.create(
            type='News Item', id=frontpage_id,
            title=title,
            description=description,
            container=portal)
        front_text = """<div class="title-1-large medium-grey-text">DevCa is the Caribbean's leading platform for exploring the intersection of digital technologies, today's biggest challenges and the future of the region. </div>
							<div class="title-1-light medium-grey-text" style="margin-top:20px;">The two day Conference and Code Sprint provides a unique opportunity for technologists, organizations, researchers and entrepreneurs across the 
							region to network while using tech innovation to solve some of the region’s biggest challenges. </div>
                     """
        frontpage.text = RichTextValue(
            front_text,
            'text/html',
            'text/x-html-safe'
        )
        portal.setDefaultPage('front-page')
        api.content.transition(frontpage, transition='publish')
        frontpage.reindexObject()
        
def create_schedule_page(portal):
    schedule_id = 'schedule'
    if schedule_id not in portal.keys():
        title = u"DevCa Schedule"
        description = u"Developing the Caribbean Schedule"
        schedule = api.content.create(
            type='News Item', id=schedule_id,
            title=title,
            description=description,
            container=portal)
        _text = """<p>Schedule for Developing the Caribbean</p>
                     """
        schedule.text = RichTextValue(
            _text,
            'text/html',
            'text/x-html-safe'
        )
        api.content.transition(schedule, transition='publish')
        schedule.reindexObject()
        
def create_image(container, src_folder, item):
    image_obj = api.content.create(
                type='Image',  # set the content type
                container=container,
                description=item['subtitle'],
                title=item['title'],
                link=item['link'])
    image_obj.image = loadimage(item['image'], src_folder)
        
def loadimage(imagename, folder):
    """ assumes the image is in a special 'content' folder """
    filename = os.path.join(folder,
                                imagename)
    return NamedBlobImage(
        data=open(filename, 'r').read(),
            filename=u'{0}'.format(imagename))