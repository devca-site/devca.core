# -*- coding: utf-8 -*-
"""Setup tests for this package."""
from devca.core.testing import DEVCA_CORE_INTEGRATION_TESTING  # noqa
from plone import api

import unittest


class TestSetup(unittest.TestCase):
    """Test that devca.core is properly installed."""

    layer = DEVCA_CORE_INTEGRATION_TESTING

    def setUp(self):
        """Custom shared utility setup for tests."""
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')

    def test_product_installed(self):
        """Test if devca.core is installed."""
        self.assertTrue(self.installer.isProductInstalled(
            'devca.core'))

    def test_browserlayer(self):
        """Test that IDevcaCoreLayer is registered."""
        from devca.core.interfaces import (
            IDevcaCoreLayer)
        from plone.browserlayer import utils
        self.assertIn(IDevcaCoreLayer, utils.registered_layers())


class TestUninstall(unittest.TestCase):

    layer = DEVCA_CORE_INTEGRATION_TESTING

    def setUp(self):
        self.portal = self.layer['portal']
        self.installer = api.portal.get_tool('portal_quickinstaller')
        self.installer.uninstallProducts(['devca.core'])

    def test_product_uninstalled(self):
        """Test if devca.core is cleanly uninstalled."""
        self.assertFalse(self.installer.isProductInstalled(
            'devca.core'))

    def test_browserlayer_removed(self):
        """Test that IDevcaCoreLayer is removed."""
        from devca.core.interfaces import IDevcaCoreLayer
        from plone.browserlayer import utils
        self.assertNotIn(IDevcaCoreLayer, utils.registered_layers())
