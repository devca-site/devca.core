from devca.core.interfaces import IDevcaCoreLayer
from plone.restapi.interfaces import ISerializeToJsonSummary
from plone.restapi.serializer.summary import DefaultJSONSummarySerializer
from zope.component import adapter
from zope.interface import implementer
from zope.interface import Interface


@implementer(ISerializeToJsonSummary)
@adapter(Interface, IDevcaCoreLayer)
class MyJSONSummarySerializer(DefaultJSONSummarySerializer):
    """Customize summary representations to include the object's portal_type.
    """

    def __init__(self, context, request):
        self.context = context
        self.request = request

    def __call__(self):
        # Get the default summary first, then modify it as needed
        summary = super(MyJSONSummarySerializer, self).__call__()

        # Include portal_type
        summary['@type'] = self.context.portal_type

        return summary